JIRA HTML Release Notes
--------

**What?**

This simple script was created to serve a simple purpose:  Get a list of issues from a JIRA project for a specific version, and output them as HTML, grouped by Issue Type (ie. Bug, Story, Task, etc).

**Why?**

There was already a myriad of ways to do this, but I felt like doing it this way.

**Dependencies?**

* Python v2.7+
* The fantastic [hammock][1] python module.
* A bottle of rum

**How?**

Edit the 'constants' at the top of the file, then:

    usage: jira-html-release-notes.py [-h] Key [Key ...] Version [Version ...]

    Generate Release Notes for a JIRA project version

    positional arguments:
      Key         JIRA project key
      Version     JIRA project version

    optional arguments:
      -h, --help  show this help message and exit

  [1]: https://github.com/streetdaddy/hammock
