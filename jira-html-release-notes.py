#!/usr/bin/env python

# A simple script for querying the JIRA 5 REST API for a list of issues for a
# version of a project, then outputting a set of HTML release notes grouped
# by Issue Type
#
# @author Miles Tillinger <miles.tillinger@visualdna.com>

import hammock
import sys
import simplejson as json
import argparse
from collections import defaultdict

# Define some 'constants' for the JIRA REST API
JIRA_URL = "<URL of your JIRA instance>"    # eg. http://jira.mycompany.com/
JIRA_API = "rest/api/latest"
JIRA_BROWSE = "browse/"
JIRA_USER = "<username>"                    #
JIRA_PASS = "<password>"                    # @TODO Keep the user/pass somewhere else

# Command line parameters
parser = argparse.ArgumentParser(description='Generate Release Notes for a JIRA project version')
parser.add_argument('project', metavar='Key', nargs='+',
                    help='JIRA project key')
parser.add_argument('version', metavar='Version', nargs='+',
                    help='JIRA project version')

args = parser.parse_args();

# Create an authenticated session
jira = hammock.Hammock(JIRA_URL + JIRA_API, auth=(JIRA_USER, JIRA_PASS))

# JQL to send to the API
query = "project = %s and fixVersion = \"%s\"" %(args.project[0], args.version[0])

# Store the response
response = jira.search.GET(params={'jql': query, 'fields': 'summary,issuetype'})

# Check for a valid response
if 200 != response.status_code:
    print >> sys.stderr, "EPIC FAIL: JIRA responded with status code %s" % response.status_code
    sys.exit(1)

# Define data structures for formatting the output
content = json.loads(response.content)
issuetype_dict = defaultdict(list)
issue_list = []
out = ''

# Group results by Issue Type (ie. Bug, Story, etc)
for issue in content['issues']:
    issuetype = issue['fields']['issuetype']['name']

    summary = ("<li><a href=\"%(jiraBrowseUrl)s%(key)s\">%(key)s</a> %(summary)s</li>" % \
        {
            'jiraBrowseUrl': JIRA_URL + JIRA_BROWSE,
            'key': issue['key'],
            'summary': issue['fields']['summary']
        })

    issue_list.append((issuetype, summary))

for k, v in issue_list:
    issuetype_dict[k].append(v)

# Output HTML
for k, items in issuetype_dict.items():
    out += "<h3>%s</h3>" % k
    out += "<ul>"
    for v in items:
        out += v
    out += "</ul>"

print out
sys.exit(0)
